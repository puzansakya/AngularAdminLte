import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';


//components goes here
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { AppheaderComponent } from './component/appheader/appheader.component';
import { AppfooterComponent } from './component/appfooter/appfooter.component';
import { AppmenuComponent } from './component/appmenu/appmenu.component';
import { AppsettingComponent } from './component/appsetting/appsetting.component';
import { CourseComponent } from './course/course.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { BatchComponent } from './batch/batch.component';
import { InstructorComponent } from './instructor/instructor.component';
import { MailComponent } from './mail/mail.component';
import { EnrollmentComponent } from './enrollment/enrollment.component';
import { BranchComponent } from './branch/branch.component';
import { StaffComponent } from './staff/staff.component';
import { PromotionComponent } from './promotion/promotion.component';
import { BankComponent } from './bank/bank.component';
import { BankVoucherComponent } from './bank-voucher/bank-voucher.component';
import { PcAllocationComponent } from './pc-allocation/pc-allocation.component';
import { PcExpensesComponent } from './pc-expenses/pc-expenses.component';
import { CalendarEventComponent } from './calendar-event/calendar-event.component';


//services goes here
import { StudentService } from "./student/student.service";
import { DashboardService } from "./dashboard/dashboard.service";
import { CourseService } from "./course/course.service";
import { MailService } from "./mail/mail.service";
import { EnquiryService } from "./enquiry/enquiry.service";
import { BatchService } from "./batch/batch.service";
import { InstructorService } from "./instructor/instructor.service";
import { EnrollmentService } from "./enrollment/enrollment.service";
import { BranchService } from "./branch/branch.service";
import { StaffService } from "./staff/staff.service";
import { BankService } from "./bank/bank.service";
import { BankVoucherService } from "./bank-voucher/bank-voucher.service";
import { PcAllocationService } from "./pc-allocation/pc-allocation.service";
import { PcExpensesService } from "./pc-expenses/pc-expenses.service";
import { AppService } from "./app.service";
import { CalendarService } from "./calendar-event/calendar.service";




@NgModule({
  declarations: [
    AppComponent,
    AppheaderComponent,
    AppfooterComponent,
    AppmenuComponent,
    AppsettingComponent,
    StudentComponent,
    CourseComponent,
    EnquiryComponent,
    BatchComponent,
    InstructorComponent,
    MailComponent,
    EnrollmentComponent,
    BranchComponent,
    StaffComponent,
    PromotionComponent,
    BankComponent,
    BankVoucherComponent,
    PcAllocationComponent,
    PcExpensesComponent,
    CalendarEventComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    NgxDatatableModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '',  redirectTo: '/dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent, pathMatch: 'full' },
      { path: 'enquiry', component: EnquiryComponent },
      { path: 'batch', component: BatchComponent },
      { path: 'course', component: CourseComponent },
      { path: 'instructor', component: InstructorComponent },
      { path: 'student', component: StudentComponent },
      { path: 'mail', component: MailComponent },
      { path: 'enrollment', component: EnrollmentComponent },
      { path: 'calendar', component: CalendarEventComponent },
      { path: 'branch', component: BranchComponent },
      { path: 'staff', component: StaffComponent },
      { path: 'promotion', component: PromotionComponent },
      { path: 'bank', component: BankComponent },
      { path: 'bankVoucher', component: BankVoucherComponent },
      { path: 'pca', component: PcAllocationComponent },
      { path: 'pce', component: PcExpensesComponent },
    ])
  ],
  providers: [
    StudentService,
    InstructorService,
    CourseService,
    EnquiryService,
    EnrollmentService,
    BranchService,
    StaffService,
    BankService,
    MailService,
    BankVoucherService,
    PcAllocationService,
    PcExpensesService,
    DashboardService,
    AppService,
    BatchService,
    CalendarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
