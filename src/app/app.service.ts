import { Injectable } from '@angular/core';
import { HttpService } from "./helper/httpService";

@Injectable()
export class AppService extends HttpService {

  isCountry: boolean = false;
  isCourse: boolean = false;
  isEnrollStatus: boolean = false;
  isBatch: boolean = false;
  isDiscount: boolean = false;


  //check if the flag is true
  //if true then stop fetching and return the array with data
  //if false then fetch the data and return with array
  countries = [];
  // courses = [];
  // enrollstatuses = [];
  // batches = [];
  // discounts = [];



  getCountry() {
    if (!this.isCountry) {
      console.log("country is false");
      this.countries = [
        { country: "Nepal" },
        { country: "India" },
        { country: "Japan" },
        { country: "America" },
        { country: "Australia" }
      ];
      this.isCountry = true;
      return this.countries;
    } else {
      console.log("country is true");
      return this.countries;

    }
  }

  getCourse() {
    // if (!this.isCourse) {
    //   this.isCourse = true;
    //   this.get("course/getAll").subscribe(res => this.courses = res.json());
    //   console.log(this.courses);
    //   return this.courses;
    // } else {
    //   return this.courses;
    // }
    return this.get("course/getAll");
  }

  getEnrollStatus() {
    // if (!this.isEnrollStatus) {
    //   this.isEnrollStatus = true;
    //   return this.get("enrollStatus/getAll").subscribe(res => this.enrollstatuses = res.json());
    // } else {
    //   return this.enrollstatuses;
    // }
    return this.get("enrollStatus/getAll");
  }

  getBatches() {
    // if (!this.isBatch) {
    //   this.isBatch = true;
    //   return this.get("batch/getAll").subscribe(res => this.batches = res.json());
    // } else {
    //   return this.batches;
    // }
    return this.get("batch/getAll");
  }

  getDiscounts() {
    // if (!this.isDiscount) {
    //   this.isDiscount = true;
    //   return this.get("discount/getAll").subscribe(res => this.discounts = res.json());
    // } else {
    //   return this.discounts;
    // }
    return this.get("discount/getAll");
  }

  getTimes() {
    return this.get("time/getAll");
  }

  getInstructors() {
    return this.get("faculty/getAll");
  }
 
}
