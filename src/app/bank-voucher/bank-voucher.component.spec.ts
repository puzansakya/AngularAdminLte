import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankVoucherComponent } from './bank-voucher.component';

describe('BankVoucherComponent', () => {
  let component: BankVoucherComponent;
  let fixture: ComponentFixture<BankVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankVoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
