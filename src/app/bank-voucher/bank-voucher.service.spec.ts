import { TestBed, inject } from '@angular/core/testing';

import { BankVoucherService } from './bank-voucher.service';

describe('BankVoucherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BankVoucherService]
    });
  });

  it('should be created', inject([BankVoucherService], (service: BankVoucherService) => {
    expect(service).toBeTruthy();
  }));
});
