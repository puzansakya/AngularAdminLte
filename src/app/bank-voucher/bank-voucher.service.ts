import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Constants } from "../helper/constants";
@Injectable()
export class BankVoucherService {

  constructor(private http:Http) { }

  getBankVouchers(){
    return this.http.get(Constants.HOME_URL+"/bankVoucser");
  }



}
