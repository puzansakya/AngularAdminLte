import { Component, OnInit } from '@angular/core';
import { BankService } from "./bank.service";

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit {

  banks = [];

  formData: any = {
    bankName: "",
    bankLocation: "",
    accountno: ""
  };

  //required for form utility
  isNewForm: boolean;
  formTitle: string;
  buttonName: string;



  constructor(public bankService: BankService) { }

  ngOnInit() {
    this.getBanks();
  }

  getBanks() {
    this.bankService.get("bank/getAll").subscribe(res => this.banks = res.json());
  }

  showEditForm(payload) {
    if (!payload) {
      return;
    }
    this.isNewForm = false;
    this.formData = payload;
    this.formTitle = "Edit bank";
    this.buttonName = "Update";
  }

  showAddForm() {
    //resets the form if edited
    if (this.banks.length) {
      this.formData = {
        bankName: "",
        bankLocation: "",
        accountno: ""
      };
    }
    this.isNewForm = true;
    this.formTitle = "Add new bank";
    this.buttonName = "Add"
  }

  save(payload) {
    if (this.isNewForm) {
      //add new course
      // this._courseService.addCourse(course).subscribe(data => this.courses.unshift(data));
      //this.courses.unshift(course);
      console.log("display save data");
      console.log(payload);
      //ajax call to add new course  this.courses = data
      this.bankService.post("bank/save", payload).subscribe(res => this.banks.unshift(res.json()));
    } else {
      //this._courseService.updateCourse(course).subscribe(data => console.log(data));
      console.log("display update data");
      console.log(payload);
      //update existing course
      //ajax call to update course
      this.bankService.post("bank/update", payload).subscribe();

    }
  }

  remove(payload) {
    this.banks.splice(this.banks.indexOf(payload), 1);
  }


}
