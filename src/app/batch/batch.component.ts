import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BatchService } from "./batch.service";
import { AppService } from "../app.service";
import { DataTables } from "../helper/datatables";
declare var $: any;

@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.css']
})
export class BatchComponent implements OnInit, AfterViewInit {

  private dt: DataTables;

  batches = [];
  //required for insert form
  courses = [];
  instructors = [];
  times = [];
  students = [];

  //required for form utility
  isNewForm: boolean;
  formTitle: string;
  buttonName: string;
  formData: any = {
    startDate: "",
    endDate: "",
    status: "",
    paymentStatus: "",
    course: {
      courseId: ""
    },
    time: {
      timeId: ""
    },
    faculty: {
      facultyId: ""
    }
  };

  constructor(public batchService: BatchService, public appService: AppService) { }

  ngOnInit() {
    this.getBatches();
    this.appService.getCourse().subscribe(res => this.courses = res.json());
    this.appService.getInstructors().subscribe(res => this.instructors = res.json());
    this.appService.getTimes().subscribe(res => this.times = res.json());

    $('.FollowUp-box').slimScroll({
      height: '250px'
    });
  }

  ngAfterViewInit(): void {
    this.dt = new DataTables("table", 0);
    this.dt.initDatatable();
  }

  getBatches() {
    this.batchService.get("batch/getAll").subscribe(res => this.batches = res.json());
  }


  showEditForm(payload) {
    if (!payload) {
      return;
    }
    this.isNewForm = false;
    this.formData = payload;
    this.formTitle = "Edit Batch";
    this.buttonName = "Update";
  }

  showAddForm() {
    //resets the form if edited
    if (this.batches.length) {
      this.formData = {
        startDate: "",
        endDate: "",
        status: "",
        paymentStatus: "",
        course: {
          courseId: "Select course"
        },
        time: {
          timeId: ""
        },
        faculty: {
          facultyId: ""
        }
      };
    }
    this.isNewForm = true;
    this.formTitle = "Add new batch";
    this.buttonName = "Add"
  }

  save(payload) {
    if (this.isNewForm) {
      //add new course
      // this._courseService.addCourse(course).subscribe(data => this.courses.unshift(data));
      //this.courses.unshift(course);
      console.log(payload);
      //ajax call to add new course  this.courses = data
      this.batchService.post("batch/save", payload).subscribe(res => {this.batches.unshift(res.json()),this.dt.reInitDatatable()});
    } else {
      //this._courseService.updateCourse(course).subscribe(data => console.log(data));
      console.log("display update data");
      console.log(payload);
      //update existing course
      //ajax call to update course

    }
  }

  remove(payload) {
    this.batches.splice(this.batches.indexOf(payload), 1);
  }

  getStudents(payload) {
    this.batchService.get("enrollment/getEnrollmentByBatchId/" + payload.batchId).subscribe(res => this.students = res.json());
  }




}
