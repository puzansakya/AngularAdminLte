import { Component, OnInit } from '@angular/core';
import { BranchService } from "./branch.service";

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {
  branches = [];

  formData: any = {
    branchName: "",
    branchLocation: "",
    status: ""
  };

  //required for form utility
  isNewForm: boolean;
  formTitle: string;
  buttonName: string;

  constructor(public branchService: BranchService) { }

  ngOnInit() {
    this.getBranches();
  }

  getBranches() {
    this.branchService.get("branch/getAll").subscribe(res => this.branches = res.json());
  }

  showEditForm(payload) {
    if (!payload) {
      return;
    }
    this.isNewForm = false;
    this.formData = payload;
    this.formTitle = "Edit Branch";
    this.buttonName = "Update";
  }

  showAddForm() {
    //resets the form if edited
    if (this.branches.length) {
      this.formData = {
        branchName: "",
        branchLocation: "",
        status: ""
      };
    }
    this.isNewForm = true;
    this.formTitle = "Add new branch";
    this.buttonName = "Add"
  }

  save(payload) {
    if (this.isNewForm) {
      //add new course
      // this._courseService.addCourse(course).subscribe(data => this.courses.unshift(data));
      //this.courses.unshift(course);
      console.log("display save data");
      console.log(payload);
      //ajax call to add new course  this.courses = data
      this.branchService.post("branch/save", payload).subscribe(res => this.branches.unshift(res.json()));
    } else {
      //this._courseService.updateCourse(course).subscribe(data => console.log(data));
      console.log("display update data");
      console.log(payload);
      //update existing course
      //ajax call to update course
      this.branchService.post("branch/update", payload).subscribe();

    }
  }

  remove(payload) {
    this.branches.splice(this.branches.indexOf(payload), 1);
  }

}
