import { Component, OnInit } from '@angular/core';
import { CalendarService } from "./calendar.service";
// import { options } from "fullcalendar";
import 'fullCalendar';
// import 'bootstrap';

declare var moment: any;
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.css']
})
export class CalendarEventComponent implements OnInit {
  // calendarOptions: options;
  errMsg;
  isPast: any = false;
  formData: any = {};
  renderUpdateEvent:any={};
  // @ViewChild(CalendarEventComponent) ucCalendar: CalendarEventComponent;

  constructor(private calendarService: CalendarService) { }

  ngOnInit() {
    this.initCal();
    var b = moment();
    console.log(b);
  }

  initCal() {
    $(function () {

      /* initialize the external events
       -----------------------------------------------------------------*/
      function init_events(ele) {
        ele.each(function () {

          // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
          // it doesn't need to have a start or end
          var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
          }

          // store the Event Object in the DOM element so we can get to it later
          $(this).data('eventObject', eventObject)

          // make the event draggable using jQuery UI
          $(this).draggable({
            zIndex: 1070,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
          })

        })
      }

      init_events($('#external-events div.external-event'));

      /* initialize the calendar
       -----------------------------------------------------------------*/
      //Date for the calendar events (dummy data)
      var date = new Date()
      var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear()
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
          today: 'today',
          month: 'month',
          week: 'week',
          day: 'day'
        },
        events: 'http://localhost:8080/Main-Module/admin/calendar/getAll',
        editable: true,
        selectable: true,
        selectHelper: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        select: function (start, end, allDay) {
          //check if the selected date is past          
          if (isPastDate(start)) {
            //display the modal with error message;
            $('#error-modal').modal('show');
          } else {
            this.errMsg = "create event success";
            console.log(this.errMsg);
            console.log(new Date(y, m, d - 2));
            this.isPast = false;
            var startDate = moment(start).format('YYYY-MM-DD');
            var m = moment().format('HH:mm:ss');
            var newStartDate = startDate + " " + m;
            console.log("Start date : " + newStartDate);

            var endDate = endDate = moment(end).subtract(1, "days").format('YYYY-MM-DD');
            var newEndDate = endDate + " " + "00:00:00";
            var a = moment(endDate);
            var b = moment(startDate);
            var dif = a.diff(b, 'days')   // =1
            var check = false;
            if (dif !== 0) {
              check = true;
              endDate = moment(end).format('YYYY-MM-DD');
              newEndDate = endDate + " " + m;
            }
            //"00:00:00"
            console.log("end date : " + newEndDate);
            console.log(dif);
            //hide the delete button
            $('#delete-event').hide();
            $('#create-event-modal').find('[name="start"]').val(newStartDate);
            $('#create-event-modal').find('[name="end"]').val(newEndDate);
            $('#create-event-modal').find('[name="timeRange"]').val(newStartDate + " - " + newEndDate);
            $('#create-event-modal').find('[name="allDay"]').val(check);
            $('#create-event-modal').find('[name="color"]').val("#3c8dbc");
            $('#add-new-event').html("Create");
            $('#modal-title').val("Crate event.");
            $('#create-event-modal').modal('show');

          }



          var startDate = moment(start).format('YYYY-MM-DD HH:mm:ss');
          var endDate = moment(end).format('YYYY-MM-DD');
        },
        eventClick: function (calEvent, jsEvent, view) {
          //change the create button text to update
          $('#add-new-event').html("Update");
          //change the modal title to update event modal
          $('#modal-title').val("Update event details.");
          //set molda form field data          
          $('#eventId').val(calEvent.id);
          //show the delete button
          $('#delete-event').show();
          $('#create-event-modal').find('[name="title"]').val(calEvent.title);
          $('#create-event-modal').find('[name="start"]').val(calEvent.start);
          $('#create-event-modal').find('[name="end"]').val(calEvent.end);
          $('#create-event-modal').find('[name="timeRange"]').val(moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss') + " - " + moment(calEvent.end).format('YYYY-MM-DD HH:mm:ss'));
          $('#create-event-modal').find('[name="description"]').val(calEvent.description);
          $('#create-event-modal').modal('show');
        },
        drop: function (date, allDay) { // this function is called when something is dropped

          // retrieve the dropped element's stored Event Object
          var originalEventObject = $(this).data('eventObject')

          // we need to copy it, so that multiple events don't have a reference to the same object
          var copiedEventObject = $.extend({}, originalEventObject)

          // assign it the date that was reported
          copiedEventObject.start = date
          copiedEventObject.allDay = allDay
          copiedEventObject.backgroundColor = $(this).css('background-color')
          copiedEventObject.borderColor = $(this).css('border-color')

          // render the event on the calendar
          // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
          $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

          // is the "remove after drop" checkbox checked?
          if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            $(this).remove()
          }

        }
      })

      /* ADDING EVENTS */
      var currColor = '#3c8dbc' //Red by default
      //Color chooser button
      var colorChooser = $('#color-chooser-btn')
      $('#color-chooser > li > a').click(function (e) {
        e.preventDefault()
        //Save color
        currColor = $(this).css('color')
        //Add color effect to button
        $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
        $('#create-event-modal').find('[name="color"]').val(currColor);
      })
      $('#add-new-event').click(function (e) {
        e.preventDefault()
        //Get value and make sure it is not null
        // var val = $('#new-event').val()
        // if (val.length == 0) {
        //   return
        // }

        //Create events
        var event = $('<div />')
        event.css({
          'background-color': currColor,
          'border-color': currColor,
          'color': '#fff'
        }).addClass('external-event')
        event.html()
        $('#external-events').prepend(event)

        //Add draggable funtionality
        init_events(event)

        //Remove event from text input
        //$('#new-event').val('')
      })

      function isPastDate(start) {
        var b = moment(start).valueOf();
        var a = moment().valueOf();

        if (a > b) {
          return true;
        }

        return false;
      }
    });
  }

  saveEvent() {
    //check if there is event id
    var id = $('#eventId').val();
    if (id == "") {

      //make payload model
      this.formData = {
        start: $('#create-event-modal').find('[name="start"]').val(),
        end: $('#create-event-modal').find('[name="end"]').val(),
        title: $('#create-event-modal').find('[name="title"]').val(),
        description: $('#create-event-modal').find('[name="description"]').val(),
        allDay: $('#create-event-modal').find('[name="allDay"]').val(),
        color: $('#create-event-modal').find('[name="color"]').val(),
      }

      //make ajax call to save the event to database
      this.calendarService.post("calendar/save", this.formData)
        .subscribe(res => {
          //render the event in the calendar
          $('#calendar').fullCalendar('renderEvent', res.json(), true);
          $('#calendar').fullCalendar('unselect');
          //reset the form
          $("#createNewEventForm")[0].reset();
        })
    } else {

      //make payload model
      this.formData = {
        id: $('#eventId').val(),        
        title: $('#create-event-modal').find('[name="title"]').val(),
        description: $('#create-event-modal').find('[name="description"]').val(),
        allDay: $('#create-event-modal').find('[name="allDay"]').val(),
        color: $('#create-event-modal').find('[name="color"]').val(),
      }

      this.renderUpdateEvent = {
        id: $('#eventId').val(),        
        start: $('#create-event-modal').find('[name="start"]').val(),
        end: $('#create-event-modal').find('[name="end"]').val(),
        title: $('#create-event-modal').find('[name="title"]').val(),
        description: $('#create-event-modal').find('[name="description"]').val(),
        allDay: $('#create-event-modal').find('[name="allDay"]').val(),
        color: $('#create-event-modal').find('[name="color"]').val(),
      }

      //make ajax call to update the event to database
      this.calendarService.post("calendar/update", this.formData)
        .subscribe(res => {
          console.log(res.json());
          //reset the form
          $("#createNewEventForm")[0].reset();
          //remove the event
          $('#calendar').fullCalendar('removeEvents', [id]);
          // then again render the event
          $('#calendar').fullCalendar('renderEvent', this.renderUpdateEvent, true);
          $('#calendar').fullCalendar('unselect');
        })
    }
  }

  deleteEvent() {
    var id = $('#eventId').val();

    this.calendarService.get("calendar/delete/" + id).subscribe(
      res => {
        $('#calendar').fullCalendar('removeEvents', [id]);
      }
    )
  }

}
