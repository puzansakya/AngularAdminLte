import { Component, OnInit } from '@angular/core';
import { AppService } from "../../app.service";
declare var $: any;
declare var submit: any;

@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppheaderComponent implements OnInit {

  header;
  token;
  username;
  profile;

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.username = $("meta[name='username']").attr("content");
    this.profile = $("meta[name='profile']").attr("content");
  }

  logout() {
    var tokenValue = $("meta[name='_csrf']").attr("content");
    var headerValue = $("meta[name='_csrf_header']").attr("content");

    this.header = headerValue;
    this.token = tokenValue;

    console.log(this.header);
    console.log(this.token);

    $("#logoutForm").submit();
  //  this.appService.logout().subscribe();
    console.log("logout pressed");

  }

}
