import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-appmenu',
  templateUrl: './appmenu.component.html',
  styleUrls: ['./appmenu.component.css']
})
export class AppmenuComponent implements OnInit {
  username;
  profile;

  constructor() { }

  ngOnInit() {
    this.username = $("meta[name='username']").attr("content");
    this.profile = $("meta[name='profile']").attr("content");
  }

}
