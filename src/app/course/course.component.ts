import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Course } from './course';
import { CourseService } from './course.service';
import { DataTables } from "../helper/datatables";
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit, AfterViewInit {
  public tableWidget: any;
  courses: Course[];  
  isNewForm: boolean;
  formTitle: string;
  newCourse: any = {};
  public dt: DataTables;
  constructor(private _courseService: CourseService) {

  }

  ngOnInit() {
    this.getCourse();    
  }

  ngAfterViewInit(): void {
    this.dt = new DataTables("table", 0);
    this.dt.initDatatable();
  }

  getCourse() {
    //this.courses = this._courseService.getCourses();
    this._courseService.getCourses().subscribe(data => this.courses = data);
  }

  showEditForm(course: Course) {
    if (!course) {
      return;
    }
    this.isNewForm = false;
    this.newCourse = course;
    this.formTitle = "Edit course data";

  }

  showAddForm() {
    //resets the form if edited
    if (this.courses.length) {
      this.newCourse = {};
    }
    this.isNewForm = true;
    this.formTitle = "Add new Course";
  }

  saveCourse(course: Course) {
    if (this.isNewForm) {
      //add new course
      this._courseService.addCourse(course).subscribe(data => { this.courses.unshift(data), this.dt.reInitDatatable() });
      //this.courses.unshift(course);
      console.log(course);
      //ajax call to add new course  this.courses = data
    } else {
      this._courseService.updateCourse(course).subscribe(data => console.log(data));
      //update existing course
      //ajax call to update course
    }
  }

  removeCourse(course: Course) {
    this._courseService.removeCourse(course);
  }


}
