import { Injectable } from '@angular/core';
import { Course } from './course';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { Constants } from "../helper/constants";


@Injectable()
export class CourseService {

  private citems = [{
    name: "advanced java",
    price: 20000
  },
  {
    name: "PHP",
    price: 18000
  }];


  constructor(private http: Http) { }


  getCourses(): Observable<Course[]> {
    //return this.citems;
    return this.http.get(Constants.HOME_URL+"course/getAll")
      .map((res: Response) => res.json());
  }

  addCourse(course: Course): Observable<Course> {    
    return this.http.post("http://localhost:8080/Test-Module/admin/course/save", course)
      .map((res: Response) => res.json());
  }
  updateCourse(course: Course): Observable<Course> {    
    return this.http.post("http://localhost:8080/Test-Module/admin/course/update", course)
      .map((res: Response) => res.json());
  }
  removeCourse(course: Course) {
    // this.citems.splice(this.citems.indexOf(course), 1);
    console.log(this.citems);
  }
}
