export interface Course {
    courseId: number;
    courseName: string;
    code: string;
    addedDate: number;
    modifiedDate: number;
    fees: number;
    status: boolean;
}   