import { Component, OnInit } from '@angular/core';
import { DashboardService } from "./dashboard.service";

declare var jquery: any;
declare var $: any;
declare var Morris: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  //declare the variable and constants here
  stats: any = [];
  followups: any = [];
  mail: any = {};


  constructor(public dashboardService: DashboardService) { }

  ngOnInit() {
    this.getStats();
    this.getLatestFollowUp();


    var visitorsData = {
      US: 398, // USA
      SA: 400, // Saudi Arabia
      CA: 1000, // Canada
      DE: 500, // Germany
      FR: 760, // France
      CN: 300, // China
      AU: 700, // Australia
      BR: 600, // Brazil
      IN: 800, // India
      GB: 320, // Great Britain
      RU: 3000 // Russia
    };



    $('#chat-box').slimScroll({
      height: '250px'
    });

    // World map by jvectormap
    $('#world-map').vectorMap({
      map: 'world_mill_en',
      backgroundColor: 'transparent',
      regionStyle: {
        initial: {
          fill: '#e4e4e4',
          'fill-opacity': 1,
          stroke: 'none',
          'stroke-width': 0,
          'stroke-opacity': 1
        }
      },
      series: {
        regions: [
          {
            values: visitorsData,
            scale: ['#92c1dc', '#ebf4f9'],
            normalizeFunction: 'polynomial'
          }
        ]
      },
      onRegionLabelShow: function (e, el, code) {
        if (typeof visitorsData[code] != 'undefined')
          el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
      }
    });

    /* Morris.js Charts */

    // Sales chart
    var area = new Morris.Area({
      element: 'revenue-chart',
      resize: true,
      data: [
        { y: '2011 Q1', item1: 2666, item2: 2666 },
        { y: '2011 Q2', item1: 2778, item2: 2294 },
        { y: '2011 Q3', item1: 4912, item2: 1969 },
        { y: '2011 Q4', item1: 3767, item2: 3597 },
        { y: '2012 Q1', item1: 6810, item2: 1914 },
        { y: '2012 Q2', item1: 5670, item2: 4293 },
        { y: '2012 Q3', item1: 4820, item2: 3795 },
        { y: '2012 Q4', item1: 15073, item2: 5967 },
        { y: '2013 Q1', item1: 10687, item2: 4460 },
        { y: '2013 Q2', item1: 8432, item2: 5713 }
      ],
      xkey: 'y',
      ykeys: ['item1', 'item2'],
      labels: ['Item 1', 'Item 2'],
      lineColors: ['#a0d0e0', '#3c8dbc'],
      hideHover: 'auto'
    });

    // Donut Chart
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ['#3c8dbc', '#f56954', '#00a65a'],
      data: [
        { label: 'Download Sales', value: 12 },
        { label: 'In-Store Sales', value: 30 },
        { label: 'Mail-Order Sales', value: 20 }
      ],
      hideHover: 'auto'
    });

    /* jQueryKnob */
    $('.knob').knob();

    // The Calender
    $('#calendar').datepicker();

  }

  getStats() {
    this.dashboardService.get("statistic").subscribe(res => {
      new Morris.Line({
        element: 'line-chart',
        resize: true,
        data: res.json().paymentStat,
        xkey: 'paymentDate',
        parseTime: false,
        ykeys: ['amount'],
        labels: ['Amount'],
        lineColors: ['#efefef'],
        lineWidth: 2,
        hideHover: 'auto',
        gridTextColor: '#fff',
        gridStrokeWidth: 0.4,
        pointSize: 4,
        pointStrokeColors: ['#efefef'],
        gridLineColor: '#efefef',
        gridTextFamily: 'Open Sans',
        gridTextSize: 10
      }),
        this.stats = res.json()
    }
    );
    // this.dashboardService.get("statistic")
  }

  getLatestFollowUp() {
    this.dashboardService.get("followup/getLatestFollowUp").subscribe(res => this.followups = res.json());
  }


  //sendmail
  sendMail() {
    //set the default sender for now
    this.mail.sendBy = "puzan"
    this.mail.isDelete=false;    
    this.dashboardService.post("mail/save", this.mail)
      .subscribe(res => {
        this.mail = {};
      });

  }

}
