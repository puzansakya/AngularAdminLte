import { Component, OnInit } from '@angular/core';
import { EnquiryService } from "./enquiry.service";
import { AppService } from "../app.service";
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css']
})
export class EnquiryComponent implements OnInit {

  enquiries = [];

  isNewForm: boolean;
  formTitle: string;
  buttonName: string;
  isShow: boolean = false;

  followMessage: any = {
    enquiryId: "",
    followMessage: "",
    follow_username:""
  };

  formData: any = {
    firstname: "",
    lastname: "",
    email: "",
    contactNo: "",
    message: "",
    course: {
      courseId: ""
    },
    enrollStatus: {
      enrollId: ""
    }
  };

  enrollment: any = {};
  // formData: any = {};

  //required for form dropdown
  courses = [];
  followups = [];
  enrollstatuses = [];
  batches = [];
  discounts = [];
  times = [];



  constructor(public enquiryService: EnquiryService, public appService: AppService) { }

  ngOnInit() {
    this.getEnquiries();
    this.appService.getCourse().subscribe(res => this.courses = res.json());
    this.appService.getBatches().subscribe(res => this.batches = res.json());
    this.appService.getDiscounts().subscribe(res => this.discounts = res.json());
    this.appService.getEnrollStatus().subscribe(res => this.enrollstatuses = res.json());
    this.appService.getTimes().subscribe(res => this.times = res.json());


    $('.FollowUp-box').slimScroll({
      height: '250px'
    });


  }

  getEnquiries() {
    this.enquiryService.get("enquiry/getAllPending").subscribe(res => this.enquiries = res.json());
  }



  showEditForm(payload) {
    if (!payload) {
      return;
    }
    this.isNewForm = false;
    this.formData = payload;
    this.formTitle = "Edit Enquiry";
    this.buttonName = "Update";

  }

  showAddForm() {
    //resets the form if edited
    this.formData = {
      firstname: "",
      lastname: "",
      email: "",
      contactNo: "",
      message: "",
      course: {
        courseId: ""
      },
      enrollStatus: {
        enrollId: ""
      }
    };
    this.isNewForm = true;
    this.formTitle = "Add new enquiry";
    this.buttonName = "Save";
  }

  save(payload) {
    if (this.isNewForm) {
      //add new course
      //this.instructorService.addCourse(course).subscribe(data => this.courses.unshift(data));
      //this.courses.unshift(course);
      console.log(payload);
      this.enquiryService.post("enquiry/save", payload).subscribe(res => this.enquiries.unshift(res.json()))
      //ajax call to add new course  this.courses = data
    } else {
      //this._courseService.updateCourse(course).subscribe(data => console.log(data));
      //update existing 
      console.log("update method");
      console.log(payload);


      //check if pending
      if (payload.enrollStatus.enrollId != 1) {
        //not pending then remove from list
        //this.enquiries.splice(this.enquiries.indexOf(payload), 1);
        //ajax call to update 
        this.enquiryService.post("enquiry/update", payload).subscribe(res => this.enquiries.splice(this.enquiries.indexOf(payload), 1));
      } else {
        this.enquiryService.post("enquiry/update", payload).subscribe();
      }

    }
  }

  delete(i) {
    //remove the item from the index after successful delete ajax method
    this.enquiries.splice(this.enquiries.indexOf(i), 1);
  }

  getAllFollowUp(payload, show) {
    this.formData = payload;
    this.enquiryService.get("followup/getFollowUp/" + payload.enquiryId).subscribe(res => this.followups = res.json());
    if (show == 1) {
      this.isShow = true;
      this.followMessage = {
        enquiryId: "",
        followMessage: "",
        follow_username:""
      };
    } else {
      this.isShow = false;
    }
  }

  saveFollowUp(payload) {
    //set the enquiry id from the formdata set by triggering the getAllfollowup
    this.followMessage.enquiryId = this.formData.enquiryId;    
    // this.followMessage.username = $("meta[name='username']").attr("content");
    this.followMessage.follow_username = "puzan";
    this.enquiryService.post("followup/save", payload).subscribe(res => this.followups.push(res.json()), this.formData.follow_up_count = this.formData.follow_up_count + 1);
  }

  showEnrollForm(payload) {
    //this method is called when enroll button is pressed
    //the enrollment is set to payload to get enquiry id and data related to enquiries
    this.formData = payload;
    this.enrollment = {};
    console.log(payload);
  }

  saveEnrollment(payload) {
    console.log("display enrollment data");
    this.enrollment.enquiryId = this.formData.enquiryId;
    this.enrollment.studentAvatar = "http://localhost:8080/Main-Module/static/img/student_img/placeholder.png";
    console.log(this.enrollment);
    //ajax call to save enrollment
    this.enquiryService.post("enrollment/store", this.enrollment).subscribe(res => this.enquiries.splice(this.enquiries.indexOf(this.formData), 1));
  }
}
