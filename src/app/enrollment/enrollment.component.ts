import { Component, OnInit,AfterViewInit } from '@angular/core';
import { EnrollmentService } from "./enrollment.service";
import { DataTables } from "../helper/datatables";
declare var $:any;
@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.css']
})

export class EnrollmentComponent implements OnInit,AfterViewInit {
  enrollments = [];

  private dt:DataTables;

  enrollment: any = {
    enrollmentId: "",
    firstName: "",
    lastName: "",
    email: "",
    discount: "",
    time: "",
    batchCode: "",
    courseName: "",
    fees: "",
    enrolledDate: "",
    amount: "",
    remAmount: "",
    paidPercentage: ""
  };

  payment: any = {
    amount: "",
    paymentType: "",
    bank: "",
    checkNo: "",
    receiptNo: "",
    enrollment: {
      enrollmentId: ""
    }
  };

  calc: any = {
    newPaidAmount: "",
    newRemPercetage: "",
    newRemAmount: ""
  };

  public tableWidget: any;

  constructor(public enrollmentService: EnrollmentService) { }

  ngOnInit() {
    this.getEnrollments();
  }

  ngAfterViewInit(): void {
    this.dt = new DataTables("table",0);
    this.dt.initDatatable();
  }

  getEnrollments() {
    this.enrollmentService.get("enrollment/getAll").subscribe(res => this.enrollments = res.json());
  }

  showPaymentForm(payload) {
    this.enrollment = payload;
    this.enrollment.paidPercentage = (this.enrollment.amount / this.enrollment.fees) * 100;
    this.payment = {
      amount: "",
      paymentType: "",
      bank: "",
      checkNo: "",
      receiptNo: "",
      enrollment: {
        enrollmentId: ""
      }
    };
    //set default calc value when payment modal is opened
    this.calc = {
      newPaidAmount: payload.amount,
      newRemPercentage: payload.paidPercentage,
      newRemAmount: payload.remAmount
    };


  }

  calculation() {
    //calculate new remaining amount
    this.calc.newPaidAmount = this.enrollment.amount + this.payment.amount;
    //console.log(this.calc.newPaidAmount);

    //calculate new remaining percentage
    this.calc.newRemPercentage = (this.calc.newPaidAmount / this.enrollment.fees) * 100;

    //to sync the progress bar of payment modal with table progress bar
    this.enrollment.paidPercentage = this.calc.newRemPercentage;

    //console.log(this.calc.newRemPercetage);

    //calculate new remaining amount
    this.calc.newRemAmount = this.enrollment.fees - this.calc.newPaidAmount;
    //console.log(this.calc.newRemAmount);


  }

  savePayment(payload) {
    //set enrollmenId from the selected enrollment while opening the payment modal
    this.payment.enrollment.enrollmentId = this.enrollment.enrollmentId;
    //to show the correct amount and paid percentage when showPaymentmodal is called
    this.enrollment.amount = this.enrollment.amount + this.payment.amount;
    this.enrollment.paidPercentage = (this.enrollment.amount/this.enrollment.fees)*100;
    this.enrollment.remAmount = this.enrollment.fees-this.enrollment.amount;

    console.log("displaying payment data");
    console.log(this.payment);
    this.enrollmentService.post("payment/save", this.payment).subscribe(res => console.log(res.json()));

  }
}
