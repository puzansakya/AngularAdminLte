import { Injectable } from '@angular/core';

// declare var jquery: any;
declare var jquery: any;
declare var $: any;


@Injectable()
export class DataTables {
    public tableWidget: any;
    private table_id;
    private order_col;

    constructor(public tableId, public orderCol) {
        this.table_id = tableId;
        this.order_col = orderCol;
    }

    public setDatatable(): void {
        let exampleId: any = $('#' + this.table_id);
        this.tableWidget = exampleId.DataTable({
            dom: 'Bfrtip',
            columnDefs: [
                {
                    targets: [0],
                    visible: false
                }
            ],
            order: [[this.order_col, "desc"]],
            buttons: [
                {
                    extend: 'copy',
                    className: 'btn btn-default btn-xs',
                    text: 'Copy',
                    exportOptions: {
                        columns: 'th:not(.noExport)'
                    }
                },
                {
                    extend: 'csv',
                    className: 'btn btn-success btn-xs',
                    text: 'CSV',
                    exportOptions: {
                        columns: 'th:not(.noExport)'
                    }
                },
                {
                    extend: 'excel',
                    className: 'btn btn-warning btn-xs',
                    text: 'Excel',
                    exportOptions: {
                        columns: 'th:not(.noExport)'
                    }
                },
                {
                    extend: 'pdf',
                    className: 'btn btn-primary btn-xs',
                    text: 'PDF',
                    exportOptions: {
                        columns: 'th:not(.noExport)'
                    }
                },
                {
                    extend: 'print',
                    className: 'btn btn-danger btn-xs',
                    text: 'Print',
                    exportOptions: {
                        columns: 'th:not(.noExport)'
                    }
                }
            ]

            //select:true,
            // dom: 'Bfrtip',    
            // buttons: [
            //   'copy', 'csv', 'excel', 'pdf', 'print'
            // ]
        });
        //   $('#example')
        //     .removeClass('display')
        //     .addClass('table table-striped table-bordered')
    }

    public reInitDatatable(): void {
        if (this.tableWidget) {
            this.tableWidget.destroy()
            this.tableWidget = null
        }
        setTimeout(() => this.setDatatable(), 0)

    }

    public initDatatable() {
        setTimeout(() => this.setDatatable(), 300);
    }

}
