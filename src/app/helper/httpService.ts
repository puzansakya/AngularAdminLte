import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Constants } from "../helper/constants";

// declare var jquery: any;
declare var $: any;


@Injectable()
export class HttpService {

    constructor(private http: Http) { }

    get(url) {
        // var token = $("meta[name='_csrf']").attr("content");
        // var header = $("meta[name='_csrf_header']").attr("content");

        // console.log("display get");
        // console.log(token);
        // console.log(header);

        // var reqHeaders = new Headers();
        // reqHeaders.append(header, token);

        // return this.http.get(Constants.HOME_URL + url, {
        //     headers: reqHeaders
        // });

        return this.http.get(Constants.HOME_URL + url);

        // return [{ fullname: "Puzan Sakya", address: {street:"janata marga",city:"kathmandu",postalcode:"00977"}, country: "Nepal" },
        // { fullname: "Nitant Rai", address: {street:"Dharan sadak",city:"Dharan",postalcode:"00977"}, country: "Pakistan" },
        // { fullname: "Ashraff Hussain", address: {street:"moriya tole",city:"Pokhara",postalcode:"00977"}, country: "China" }];
    }

    post(url, payload) {
        // var token = $("meta[name='_csrf']").attr("content");
        // var header = $("meta[name='_csrf_header']").attr("content");

        // console.log("display post");
        // console.log(token);
        // console.log(header);

        // var headers = new Headers();
        // headers.append(header, token);

        // return this.http.post(Constants.HOME_URL + url, payload, {
        //     headers: headers
        // });


        return this.http.post(Constants.HOME_URL + url, payload);


    }


}
