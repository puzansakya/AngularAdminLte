import { Component, OnInit } from '@angular/core';
import { InstructorService } from "./instructor.service";

@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.html',
  styleUrls: ['./instructor.component.css']
})

export class InstructorComponent implements OnInit {

  instructors = [];

  isNewForm: boolean;
  formTitle: string;
  buttonName: string;
  //formData: any = { fullname: "", address: { street: "", city: "", postalcode: "" }, country: "" };
  formData: any = {};  


  constructor(public instructorService: InstructorService) { }

  ngOnInit() {
    this.getInstructor();
  }

  getInstructor() {
    this.instructorService.get("faculty/getAll").subscribe(res => this.instructors = res.json());
    //this.instructors = this.instructorService.getInstructors();

  }


  showEditForm(instructor) {
    if (!instructor) {
      return;
    }
    this.isNewForm = false;
    this.formData = instructor;
    this.formTitle = "Edit course data";
    this.buttonName = "Update";

  }

  showAddForm() {
    //resets the form if edited
    if (this.instructors.length) {
      //this.formData = { fullname: "", address: { street: "", city: "", postalcode: "" }, country: "" };
      this.formData = {};
    }
    this.isNewForm = true;
    this.formTitle = "Add new";
    this.buttonName = "Save";
  }

  save(instructor) {
    if (this.isNewForm) {
      //add new course
      //this.instructorService.addCourse(course).subscribe(data => this.courses.unshift(data));
      //this.courses.unshift(course);
      console.log(instructor);
      this.instructorService.post("faculty/save", instructor).subscribe(res => this.instructors.unshift(res.json()))
      //ajax call to add new course  this.courses = data
    } else {
      //this._courseService.updateCourse(course).subscribe(data => console.log(data));
      //update existing 
      console.log("update method" + instructor);
      //ajax call to update 
      this.instructorService.post("faculty/update", instructor).subscribe(res => console.log(res.json()));

    }
  }

  delete(i) {
    //remove the item from the index after successful delete ajax method
    this.instructors.splice(this.instructors.indexOf(i), 1);
  }

}
