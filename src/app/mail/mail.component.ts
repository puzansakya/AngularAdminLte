import { Component, OnInit } from '@angular/core';
import { MailService } from "./mail.service";
declare var $: any;
@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.css']
})
export class MailComponent implements OnInit {

  mails: any = [];
  mail: any = {};
  constructor(public mailService: MailService) { }

  ngOnInit() {
    this.getAllMail();
    $(function () {
      //Add text editor
      $("#compose-textarea").wysihtml5();
    });
  }

  getAllMail() {
    this.mailService.get("mail/getAll").subscribe(res => this.mails = res.json());
  }

  //sendmail
  sendMail() {
    //set the default sender for now
    this.mail.sendBy = "puzan"
    this.mail.isDelete = false;
    this.mailService.post("mail/save", this.mail)
      .subscribe(res => {
        this.mails.unshift(res.json());
      });

  }

}
