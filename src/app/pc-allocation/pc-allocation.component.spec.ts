import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcAllocationComponent } from './pc-allocation.component';

describe('PcAllocationComponent', () => {
  let component: PcAllocationComponent;
  let fixture: ComponentFixture<PcAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
