import { Component, OnInit } from '@angular/core';
import { PcAllocationService } from "./pc-allocation.service";

@Component({
  selector: 'app-pc-allocation',
  templateUrl: './pc-allocation.component.html',
  styleUrls: ['./pc-allocation.component.css']
})
export class PcAllocationComponent implements OnInit {
  pcas = [];

  formData: any = {
    amount: "",
    releasedBy: {
      username: "puzan"
    }
  };

  //required for form utility
  isNewForm: boolean;
  formTitle: string;
  buttonName: string;



  constructor(public pcaService: PcAllocationService) { }

  ngOnInit() {
    this.getPCA();
  }

  getPCA() {
    this.pcaService.get("pca/getAll").subscribe(res => this.pcas = res.json());
  }

  showEditForm(payload) {
    if (!payload) {
      return;
    }
    this.isNewForm = false;
    this.formData = payload;
    this.formTitle = "Edit Petty Cash Allocation";
    this.buttonName = "Update";
  }

  showAddForm() {
    //resets the form if edited
    if (this.pcas.length) {
      this.formData = {
        amount: "",
        releasedBy: {
          username:"puzan"
        }
      };
    }
    this.isNewForm = true;
    this.formTitle = "Add Petty Cash Allocation";
    this.buttonName = "Add"
  }

  save(payload) {
    if (this.isNewForm) {
      //add new course
      // this._courseService.addCourse(course).subscribe(data => this.courses.unshift(data));
      //this.courses.unshift(course);
      console.log("display save data");
      console.log(payload);
      //ajax call to add new course  this.courses = data
      this.pcaService.post("pca/save", payload).subscribe(res => this.pcas.unshift(res.json()));
    } else {
      //this._courseService.updateCourse(course).subscribe(data => console.log(data));
      console.log("display update data");
      console.log(payload);
      //update existing course
      //ajax call to update course
      this.pcaService.post("pca/update", payload).subscribe();

    }
  }

  remove(payload) {
    this.pcas.splice(this.pcas.indexOf(payload), 1);
  }


}
