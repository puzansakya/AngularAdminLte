import { TestBed, inject } from '@angular/core/testing';

import { PcAllocationService } from './pc-allocation.service';

describe('PcAllocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PcAllocationService]
    });
  });

  it('should be created', inject([PcAllocationService], (service: PcAllocationService) => {
    expect(service).toBeTruthy();
  }));
});
