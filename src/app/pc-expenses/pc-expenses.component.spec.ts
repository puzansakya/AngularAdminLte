import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcExpensesComponent } from './pc-expenses.component';

describe('PcExpensesComponent', () => {
  let component: PcExpensesComponent;
  let fixture: ComponentFixture<PcExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
