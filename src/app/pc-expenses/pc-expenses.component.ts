import { Component, OnInit } from '@angular/core';
import { PcExpensesService } from "./pc-expenses.service";

@Component({
  selector: 'app-pc-expenses',
  templateUrl: './pc-expenses.component.html',
  styleUrls: ['./pc-expenses.component.css']
})
export class PcExpensesComponent implements OnInit {

  pces = [];

  constructor(public pceService: PcExpensesService) { }

  ngOnInit() {
    this.getPCES();
  }

  getPCES() {
    this.pceService.get("pce/getAll").subscribe(res => this.pces = res.json());
  }

  showAddForm() {

  }

  showEditForm() {

  }

}
