import { TestBed, inject } from '@angular/core/testing';

import { PcExpensesService } from './pc-expenses.service';

describe('PcExpensesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PcExpensesService]
    });
  });

  it('should be created', inject([PcExpensesService], (service: PcExpensesService) => {
    expect(service).toBeTruthy();
  }));
});
