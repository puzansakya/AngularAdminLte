import { Component, OnInit } from '@angular/core';
import { StaffService } from "./staff.service";
@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
  staffs = [];
  constructor(public staffService: StaffService) { }

  ngOnInit() {
    this.getStaffs();
  }

  getStaffs() {
    this.staffService.get("staff/getAll").subscribe(res => this.staffs = res.json());
  }

  showAddForm() {

  }


}
