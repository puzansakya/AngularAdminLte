import { Component, OnInit } from '@angular/core';
import { StudentService } from './student.service';
import { student } from './student';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students = [];
  s: student = new student();  


  // columns = [
  //   { prop: 'Id' },
  //   { name: 'Title' },
  //   { name: 'Body' }
  // ];

  constructor(public studentService: StudentService) {
  }

  ngOnInit() {
    this.studentService.get("student/getAll").subscribe(data => this.students = data.json());
    //this.students = this.studentService.getStudents();
  }

  save(s: student) {
    console.log("addnew pressed");
    console.log(s);
    this.students.push(s);
  }

  edit() {
    console.log("edit pressed");
  }

  delete() {
    console.log("delete pressed");
  }

  exportToCsv() {
    console.log("export to csv");
  }
}
